package application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Scanner;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import application.controller.GameOverviewController;
import application.controller.RootLayoutController;
import application.controller.SplashScreenDialogController;
import application.model.Game;
import application.model.Player;
import application.util.DateUtil;
import application.util.ResourceIO;

public class Main extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;
    private ResourceIO resourceIO;
    
    private ObservableList<Game> gameList = FXCollections.observableArrayList();
    
    public Main() {
         //Uncomment if I need to clear the file location of the logs folder. 
//        Preferences preferences = Preferences.userNodeForPackage(Main.class);
//        try {
//			preferences.clear();
//		} catch (BackingStoreException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
    }

    public ObservableList<Game> getGameData() {
        return gameList;
    }
    
    public ObservableList<Player> getPlayerData(int index) {
    	return gameList.get(index).getPlayerList();
    }

    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("AddressApp");
        
        //this.primaryStage.getIcons().add(new Image("Really need icon"));
        
        if(!doesPreferenceKeyExist("filePath")) {
        	showSplashScreenDialog();
        }
        initRootLayout();
        showGameOverview();
    }
    
    public void buildGameData(File filePath) {
		Scanner textFile = new Scanner(readFiles(filePath));
		gameList.clear();
		while(textFile.hasNextLine()) {
			ObservableList<Player> playerList = FXCollections.observableArrayList();
			String lineOfGameData = textFile.nextLine();
			String [] gameData = lineOfGameData.split("\\t", -1);
			String [] playersData = gameData[0].split("\\|");
			Game tempGame = new Game();
			
			for (String player : playersData) {
				Player tempPlayer = new Player(); 
				String [] playerData = player.split(":", -1);
				for (int i = 0; i < playerData.length; i++) {
					switch (i) {
					case 0: tempPlayer.setName  (playerData[i].equals("") ? "?" : playerData[i]); break;
					case 1: tempPlayer.setTeam  (playerData[i].equals("") ? "?" : playerData[i]); break;
					case 2: tempPlayer.setPoints(playerData[i].equals("") ? "?" : playerData[i]); break;
					case 3: tempPlayer.setRank  (playerData[i].equals("") ? "?" : playerData[i]); break;
					case 4: tempPlayer.setUserID(playerData[i].equals("") ? "?" : playerData[i]); break;
					case 5: tempPlayer.setClanID(playerData[i].equals("") ? "?" : playerData[i]); break;
					default: break;
					}
				}
				if(playerData.length == 7) {
					tempPlayer.setInactive(playerData[6].equals("") ? "?":playerData[6]);
				}
				else {
					tempPlayer.setInactive("?"); 
				}
				playerList.add(tempPlayer);
			}

			tempGame.setPlayerList(playerList);
			// data 0, id 1, ts 2 and 3, duration 4, server 5, name 6, board 7, [admin 8, tourney 9, clan 10], events 11, klass 12, custom 13
			for(int i = 1; i < gameData.length; i++) {
				switch (i) {
				case 1: tempGame.setGameID(gameData[i].equals("") ? "?":gameData[i]); break;
				case 2: 
					tempGame.setTimestamp(gameData[i].equals("") ? 
							LocalDateTime.of(2014, 10, 27, 0, 0, 0) : 
								DateUtil.parse(gameData[i]));
					break;
				case 3: tempGame.setDuration(gameData[i].equals("") ? "?":gameData[i]); break;
				case 4: tempGame.setServerID(gameData[i].equals("") ? "?":gameData[i]); break;
				case 5: tempGame.setServerName(gameData[i].equals("") ? "?":gameData[i]); break;
				case 6: tempGame.setBoard(gameData[i].equals("") ? "?":gameData[i]); break;
				case 7: tempGame.setAdminID(gameData[i].equals("") ? "?":gameData[i]); break;
				case 8:
					if(gameData[i].equals("")) {
						tempGame.setTourneyID("?"); 
						tempGame.setTourneyName("?"); 
					}
					else {
						String [] TourneyIdAndNameSeperator = gameData[i].split(":",-1);
						tempGame.setTourneyID(TourneyIdAndNameSeperator[0].equals("") ? "?":TourneyIdAndNameSeperator[0]); 
						tempGame.setTourneyName(TourneyIdAndNameSeperator[1].equals("") ? "?":TourneyIdAndNameSeperator[1]);
					}
					break;
				case 9:
					if(gameData[i].equals("")) {
						tempGame.setHostingClanID("?");
						tempGame.setHostingClanName("?");	
					}
					else {
						String [] clanIdAndNameSeperator = gameData[i].split(":",-1);
						tempGame.setHostingClanID(clanIdAndNameSeperator[0].equals("") ? "?" : clanIdAndNameSeperator[0]); 
						tempGame.setHostingClanName(clanIdAndNameSeperator[1].equals("") ? "?" : clanIdAndNameSeperator[1]);
					}
					break;
				case 10: tempGame.setEvents(gameData[i].equals("") ? "?":gameData[i]); break;
				case 11: tempGame.setKlass(gameData[i].equals("") ? "?":gameData[i]); break;
				case 12: tempGame.setCustom(gameData[i].equals("") ? "?":gameData[i]); break;
				default: break;
				}
			}
			gameList.add(tempGame);
		}
	}

	public String readFiles(File filePath) {
		try(BufferedReader br = new BufferedReader(new FileReader(filePath))) {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}

			return sb.toString();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}
    
    public void initRootLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            Screen screen = Screen.getPrimary();
            Rectangle2D screenDimension = screen.getVisualBounds();
            Scene scene = new Scene(rootLayout, screenDimension.getWidth()/1.2, screenDimension.getHeight()/1.2);
            primaryStage.setScene(scene);

            RootLayoutController controller = loader.getController();
            controller.setMainApp(this);

            primaryStage.show();
        } 
        catch (IOException e) {
            e.printStackTrace();
        }
        
        File file = getPreferenceKey("filePath");
        if (file != null) {
            resourceIO = new ResourceIO(file.getAbsolutePath() + "/");
        }
    }
    
    public void showGameOverview() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/GameOverview.fxml"));
            AnchorPane gameOverview = (AnchorPane) loader.load();

            rootLayout.setCenter(gameOverview);

            GameOverviewController controller = loader.getController();
            controller.setMainApp(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void showSplashScreenDialog() {
    	try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/SplashScreenDialog.fxml"));
            BorderPane page = (BorderPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setOnCloseRequest(e -> System.exit(0));
            dialogStage.setTitle("Default File Location");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            SplashScreenDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this);

            //dialogStage.getIcons().add(new Image("Really need a icon"));

            dialogStage.showAndWait();

        } 
    	catch (IOException e) {
            e.printStackTrace();
        }
    }

    public File getPreferenceKey(String key) {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);
        String filePath = prefs.get(key, null);
        if (filePath != null) {
            return new File(filePath);
        } 
        else {
            return null;
        }
    }

    public void setPreferenceKey(String key, String value) {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);
        if (value != null) {
            prefs.put("filePath", value);
        } 
        else {
            prefs.remove("filePath");
        }
    }
    
    public boolean doesPreferenceKeyExist(String key) {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);
        return prefs.get(key, null) != null;
    }
 
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}