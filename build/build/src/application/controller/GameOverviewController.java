package application.controller;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import application.Main;
import application.model.Game;
import application.model.Player;

public class GameOverviewController {
	@FXML
	private TableView<Game> gameTable;
	@FXML
	private TableView<Player> playerTable;
	@FXML
	private TableColumn<Player, String> nameColumn;
	@FXML
	private TableColumn<Player, String> teamColumn;
	@FXML
	private TableColumn<Player, String> pointsColumn;
	@FXML
	private TableColumn<Player, String> rankColumn;
	@FXML
	private TableColumn<Player, String> userIDColumn;
	@FXML
	private TableColumn<Player, String> clanIDColumn;
	@FXML
	private TableColumn<Player, String> inactiveColumn;
	@FXML
	private TableColumn<Game, String> gameIDColumn;
	@FXML
	private TableColumn<Game, LocalDateTime> timestampColumn;
	@FXML
	private TableColumn<Game, String> durationColumn;
	@FXML
	private TableColumn<Game, String> serverIDColumn;
	@FXML
	private TableColumn<Game, String> serverNameColumn;
	@FXML
	private TableColumn<Game, String> boardColumn;
	@FXML
	private TableColumn<Game, String> adminIDColumn;
	@FXML
	private TableColumn<Game, String> tourneyIDColumn;
	@FXML
	private TableColumn<Game, String> tourneyNameColumn;
	@FXML
	private TableColumn<Game, String> hostingClanIDColumn;
	@FXML
	private TableColumn<Game, String> hostingClanNameColumn;
	@FXML
	private TableColumn<Game, String> eventsColumn;
	@FXML
	private TableColumn<Game, String> klassColumn;
	@FXML
	private TableColumn<Game, String> customColumn;
	@FXML
	private TextField gameIDFilterField;
	@FXML
	private TextField timestampFilterField;
	@FXML
	private TextField durationFilterField;
	@FXML
	private TextField serverIDFilterField;
	@FXML
	private TextField serverNameFilterField;
	@FXML
	private TextField boardFilterField;
	@FXML
	private TextField adminIDFilterField;
	@FXML
	private TextField tourneyIDFilterField;
	@FXML
	private TextField tourneyNameFilterField;
	@FXML
	private TextField hostingClanIDFilterField;
	@FXML
	private TextField hostingClanNameFilterField;
	@FXML
	private TextField eventsFilterField;
	@FXML
	private TextField klassFilterField;
	@FXML
	private TextField customFilterField;

	@FXML
	private DatePicker datePicker; 

	private Main main;

	public GameOverviewController() {
	}

	@FXML
	private void initialize() {
		// Initialize the person table with the two columns.
		nameColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().nameProperty());
		teamColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().teamProperty());
		pointsColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().pointsProperty());
		rankColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().rankProperty());
		userIDColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().userIDProperty());
		clanIDColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().clanIDProperty());
		inactiveColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().inactiveProperty());
		gameIDColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().gameIDProperty());
		timestampColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().timestampProperty());
		durationColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().durationProperty());
		serverIDColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().serverIDProperty());
		serverNameColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().serverNameProperty());
		boardColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().boardProperty());
		adminIDColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().adminIDProperty());
		tourneyIDColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().tourneyIDProperty());
		tourneyNameColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().tourneyNameProperty());
		hostingClanIDColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().hostingClanIDProperty());
		hostingClanNameColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().hostingClanNameProperty());
		eventsColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().eventsProperty());
		klassColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().klassProperty());
		customColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().customProperty());

		nameColumn.setMinWidth(textLength(nameColumn.getText()));
		teamColumn.setMinWidth(textLength(teamColumn.getText()));
		pointsColumn.setMinWidth(textLength(pointsColumn.getText()));
		rankColumn.setMinWidth(textLength(rankColumn.getText()));
		userIDColumn.setMinWidth(textLength(userIDColumn.getText()));
		clanIDColumn.setMinWidth(textLength(clanIDColumn.getText()));
		inactiveColumn.setMinWidth(textLength(inactiveColumn.getText()));
		gameIDColumn.setMinWidth(textLength(gameIDColumn.getText()));
		timestampColumn.setMinWidth(textLength(timestampColumn.getText()));
		durationColumn.setMinWidth(textLength(durationColumn.getText()));
		serverIDColumn.setMinWidth(textLength(serverIDColumn.getText()));
		serverNameColumn.setMinWidth(textLength(serverNameColumn.getText()));
		boardColumn.setMinWidth(textLength(boardColumn.getText()));
		adminIDColumn.setMinWidth(textLength(adminIDColumn.getText()));
		tourneyIDColumn.setMinWidth(textLength(tourneyIDColumn.getText()));
		tourneyNameColumn.setMinWidth(textLength(tourneyNameColumn.getText()));
		hostingClanIDColumn.setMinWidth(textLength(hostingClanIDColumn.getText()));
		hostingClanNameColumn.setMinWidth(textLength(hostingClanNameColumn.getText()));
		eventsColumn.setMinWidth(textLength(eventsColumn.getText()));
		klassColumn.setMinWidth(textLength(klassColumn.getText()));
		customColumn.setMinWidth(textLength(customColumn.getText()));

		gameTable.getSelectionModel().selectedItemProperty().addListener(
				(observable, oldValue, newValue) -> {
					playerTable.setItems(main.getPlayerData(gameTable.getSelectionModel().getSelectedIndex()));
				});
	}

	@FXML 
	private void handleDateSelection() {
		LocalDate tempDate = datePicker.getValue();

		String datePattern = "yyyy-MM-dd";
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(datePattern);
		String dateValue = dateFormatter.format(tempDate);
		File filePath = new File(main.getPreferenceKey("filePath").getPath() + "/" + dateValue + ".txt");
		if(filePath.exists() && !filePath.isDirectory()) {
			main.buildGameData(filePath);
		}
	}

	// Ugly way to do this, but it works for right now. 
	public double textLength(String columnText) {
		Text text = new Text(columnText);
		text.setStyle("-fx-font-size: 20pt; -fx-font-family: \"Segoe UI Light\";");
		new Scene( new Group( text ) );  
		text.applyCss(); 
		return text.getLayoutBounds().getWidth() + 30;
	}

	public void setMainApp(Main main) {
		this.main = main;

		gameTable.setItems(main.getGameData());
		// 1. Wrap the ObservableList in a FilteredList (initially display all data).
		FilteredList<Game> filteredData = new FilteredList<>(main.getGameData(), p -> true);

		gameIDFilterField.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(game -> {
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(game.getGameID().toLowerCase().contains(lowerCaseFilter)) {
					return true; 
				}
				else {
					return false;
				}
			});
		});
		timestampFilterField.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(game -> {
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				String datePattern = "yyyy-MM-dd";
				DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(datePattern);
				String dateValue = dateFormatter.format(game.getTimestamp());
				
				if(dateValue.toLowerCase().contains(lowerCaseFilter)) {
					return true; 
				}
				else {
					return false;
				}
			});
		});
		durationFilterField.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(game -> {
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(game.getDuration().toLowerCase().contains(lowerCaseFilter)) {
					return true; 
				}
				else {
					return false;
				}
			});
		});
		serverIDFilterField.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(game -> {
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(game.getServerID().toLowerCase().contains(lowerCaseFilter)) {
					return true; 
				}
				else {
					return false;
				}
			});
		});
		serverNameFilterField.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(game -> {
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(game.getServerName().toLowerCase().contains(lowerCaseFilter)) {
					return true; 
				}
				else {
					return false;
				}
			});
		});
		boardFilterField.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(game -> {
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(game.getBoard().toLowerCase().contains(lowerCaseFilter)) {
					return true; 
				}
				else {
					return false;
				}
			});
		});
		adminIDFilterField.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(game -> {
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(game.getAdminID().toLowerCase().contains(lowerCaseFilter)) {
					return true; 
				}
				else {
					return false;
				}
			});
		});
		tourneyIDFilterField.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(game -> {
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(game.getTourneyID().toLowerCase().contains(lowerCaseFilter)) {
					return true; 
				}
				else {
					return false;
				}
			});
		});
		tourneyNameFilterField.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(game -> {
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(game.getTourneyName().toLowerCase().contains(lowerCaseFilter)) {
					return true; 
				}
				else {
					return false;
				}
			});
		});
		hostingClanIDFilterField.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(game -> {
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(game.getHostingClanID().toLowerCase().contains(lowerCaseFilter)) {
					return true; 
				}
				else {
					return false;
				}
			});
		});
		hostingClanNameFilterField.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(game -> {
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(game.getHostingClanName().toLowerCase().contains(lowerCaseFilter)) {
					return true; 
				}
				else {
					return false;
				}
			});
		});
		eventsFilterField.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(game -> {
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(game.getEvents().toLowerCase().contains(lowerCaseFilter)) {
					return true; 
				}
				else {
					return false;
				}
			});
		});
		klassFilterField.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(game -> {
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(game.getKlass().toLowerCase().contains(lowerCaseFilter)) {
					return true; 
				}
				else {
					return false;
				}
			});
		});
		customFilterField.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(game -> {
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(game.getCustom().toLowerCase().contains(lowerCaseFilter)) {
					return true; 
				}
				else {
					return false;
				}
			});
		});

		SortedList<Game> sortedData = new SortedList<>(filteredData);
		sortedData.comparatorProperty().bind(gameTable.comparatorProperty());
		gameTable.setItems(sortedData);
	}
}