package application.model;

import java.time.LocalDateTime;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import application.util.DateUtil;

public class Game {
	private ObservableList<Player> playerList;
	private StringProperty gameID; // The game ID of the given game. 
	private ObjectProperty<LocalDateTime> timestamp; // The date that the given game happened. 
	private StringProperty duration; // The number of seconds that the given game took. 
	private StringProperty serverID; // The server ID of the given game server 
	private StringProperty serverName; // The sever name of the given game server. 
	private StringProperty board; // The game type of the given game. 
	private StringProperty adminID; // The admin ID of the given tournament.  
	private StringProperty tourneyID; // The tournament ID.
	private StringProperty tourneyName; // The tournament name.
	private StringProperty hostingClanID; // The server ID of the given game 
	private StringProperty hostingClanName; // The name of the clan hosting the tournament
	// Events are broken up by |'s for each round in a ladder tourney.  The following events are logged, comma delimited: 
	// T60 - Timer ran out after 60 seconds. 
	// 1s - 1 surrendered 
	// 1r - 1 revolted 
	// 1d - 1 died 
	// 1e2 - 1 eliminated 2 
	private StringProperty events;  
	private StringProperty klass; // The ship type
	private StringProperty custom; // Custom is a urlencoded list of game parameters.  For example: "planets=23&production=100&teams=4&symmetry=0&speed=5" 

	public Game() {
		this.playerList = playerList;
		this.gameID = new SimpleStringProperty();
		this.timestamp = new SimpleObjectProperty<LocalDateTime>();
		this.duration = new SimpleStringProperty();
		this.serverID = new SimpleStringProperty();
		this.serverName = new SimpleStringProperty();
		this.board = new SimpleStringProperty();
		this.adminID = new SimpleStringProperty();
		this.tourneyID = new SimpleStringProperty();
		this.tourneyName = new SimpleStringProperty();
		this.hostingClanID = new SimpleStringProperty();
		this.hostingClanName = new SimpleStringProperty();
		this.events = new SimpleStringProperty();
		this.klass = new SimpleStringProperty();
		this.custom = new SimpleStringProperty();
	}

	public Game(ObservableList<Player> playerList, String gameID,
			LocalDateTime timestamp, String duration, String serverID, String serverName,
			String board, String admin, String tourneyID, String tourneyName,
			String hostingClanID, String hostingClanName, String events, String klass, 
			String custom) {
		this.playerList = playerList;
		this.gameID = new SimpleStringProperty(gameID);
		this.timestamp = new SimpleObjectProperty<LocalDateTime>(LocalDateTime.of(2014, 10, 27, 0, 0, 0));
		this.duration = new SimpleStringProperty(duration);
		this.serverID = new SimpleStringProperty(serverID);
		this.serverName = new SimpleStringProperty(serverName);
		this.board = new SimpleStringProperty(board);
		this.adminID = new SimpleStringProperty(admin);
		this.tourneyID = new SimpleStringProperty(tourneyID);
		this.tourneyName = new SimpleStringProperty(tourneyName);
		this.hostingClanID = new SimpleStringProperty(hostingClanID);
		this.hostingClanName = new SimpleStringProperty(hostingClanName);
		this.events = new SimpleStringProperty(events);
		this.klass = new SimpleStringProperty(klass);
		this.custom = new SimpleStringProperty(custom);
	}
	// ******************** Getters ********************
	public ObservableList<Player> getPlayerList() {return playerList;}
	public String getGameID() {return gameID.get();}
	public LocalDateTime getTimestamp() {return timestamp.get();}
	public String getDuration() {return duration.get();}
	public String getServerID() {return serverID.get();}
	public String getServerName() {return serverName.get();}
	public String getBoard() {return board.get();}
	public String getAdminID() {return adminID.get();}
	public String getTourneyID() {return tourneyID.get();}
	public String getTourneyName() {return tourneyName.get();}
	public String getHostingClanID() {return hostingClanID.get();}
	public String getHostingClanName() {return hostingClanName.get();}
	public String getEvents() {return events.get();}
	public String getKlass() {return klass.get();}
	public String getCustom() {return custom.get();}
	// ******************** Setters ********************
	public void setPlayerList(ObservableList<Player> playerList) {this.playerList = playerList;}
	public void setGameID(String gameID) {this.gameID.set(gameID);}
	public void setTimestamp(LocalDateTime timestamp) {this.timestamp.set(timestamp);}
	public void setDuration(String duration) {this.duration.set(duration);}
	public void setServerID(String serverID) {this.serverID.set(serverID);}
	public void setServerName(String serverName) {this.serverName.set(serverName);}
	public void setBoard(String board) {this.board.set(board);}
	public void setAdminID(String adminID) {this.adminID.set(adminID);}
	public void setTourneyID(String tourneyID) {this.tourneyID.set(tourneyID);}
	public void setTourneyName(String tourneyName) {this.tourneyName.set(tourneyName);}
	public void setHostingClanID(String hostingClanID) {this.hostingClanID.set(hostingClanID);}
	public void setHostingClanName(String hostingClanName) {this.hostingClanName.set(hostingClanName);}
	public void setEvents(String events) {this.events.set(events);}
	public void setKlass(String klass) {this.klass.set(klass);}
	public void setCustom(String custom) {this.custom.set(custom);}
	// ******************** Properties ********************
	public StringProperty gameIDProperty() {return gameID;}
	public ObjectProperty<LocalDateTime> timestampProperty() {return timestamp;}
	public StringProperty durationProperty() {return duration;}
	public StringProperty serverIDProperty() {return serverID;}
	public StringProperty serverNameProperty() {return serverName;}
	public StringProperty boardProperty() {return board;}
	public StringProperty adminIDProperty() {return adminID;}
	public StringProperty tourneyIDProperty() {return tourneyID;}
	public StringProperty tourneyNameProperty() {return tourneyName;}
	public StringProperty hostingClanIDProperty() {return hostingClanID;}
	public StringProperty hostingClanNameProperty() {return hostingClanName;}
	public StringProperty eventsProperty() {return events;}
	public StringProperty klassProperty() {return klass;}
	public StringProperty customProperty() {return custom;}
	public String toString() {
		return "Player List: " + playerList.toString() + 
				" Game ID: " + gameID.get() + 
				" Timestamp: " + DateUtil.format(timestamp.get()) +
				" Duration: " + duration.get() +
				" ServerID: " + serverID.get() +
				" Server Name: " + serverName.get() +
				" Board: " + board.get() +
				" Admin ID: " + adminID.get() +
				" Tourney ID: " + tourneyID.get() +
				" Tourney Name: " + tourneyName.get() +
				" Hosting Clan ID: " + hostingClanID.get() +
				" Hosting Clan Name: " + hostingClanName.get() +
				" Events: " + events.get() +
				" Klass: " + klass.get() +
				" Custom: " + custom.get();
	}
}
