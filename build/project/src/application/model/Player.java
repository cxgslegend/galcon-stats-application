package application.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Player {
	private StringProperty name; // The name of the given player. 
	private StringProperty team; // The great and powerful Phil hath commanded that thou shalt not care about this property. 
	private StringProperty points; // 1 is a win, and 0 is a loss. 
	private StringProperty rank; // The rank of the given player as a hex three digit hex value. 
	private StringProperty userID; // The user ID of a given player. It stays the same even if the player changes his/her name. 
	private StringProperty clanID; // The clan ID of a given player. 
	private StringProperty inactive; // 0 is active, and 1 is inactive. The field was not included in the first .txt files. 

	public Player() {
		this.name = new SimpleStringProperty(); 
		this.team = new SimpleStringProperty(); 
		this.points = new SimpleStringProperty();  
		this.rank = new SimpleStringProperty(); 
		this.userID = new SimpleStringProperty();
		this.clanID = new SimpleStringProperty();
		this.inactive = new SimpleStringProperty();
	}

	public Player(String name, String team, String points, String rank,
			String userID, String clanID, String inactive) {
		this.name = new SimpleStringProperty(name); 
		this.team = new SimpleStringProperty(team); 
		this.points = new SimpleStringProperty(points);  
		this.rank = new SimpleStringProperty(rank); 
		this.userID = new SimpleStringProperty(userID);
		this.clanID = new SimpleStringProperty(clanID);
		this.inactive = new SimpleStringProperty(inactive);
	}

	// ******************** Getters ********************
	public String getName() {return name.get();}
	public String getTeam() {return team.get();}
	public String getPoints() {return points.get();}
	public String getRank() {return rank.get();}
	public String getUserID() {return userID.get();}
	public String getClanID() {return clanID.get();}
	public String getInactive() {return inactive.get();}
	// ******************** Setters ********************
	public void setName(String name) {this.name.set(name);}
	public void setTeam(String team) {this.team.set(team);}
	public void setPoints(String points) {this.points.set(points);}
	public void setRank(String rank) {this.rank.set(rank);}
	public void setUserID(String userID) {this.userID.set(userID);}
	public void setClanID(String clanID) {this.clanID.set(clanID);}
	public void setInactive(String inactive) {this.inactive.set(inactive);}
	// ******************** Properties ********************
	public StringProperty nameProperty() {return name;}
	public StringProperty teamProperty() {return team;}
	public StringProperty pointsProperty() {return points;}
	public StringProperty rankProperty() {return rank;}
	public StringProperty userIDProperty() {return userID;}
	public StringProperty clanIDProperty() {return clanID;}
	public StringProperty inactiveProperty() {return inactive;}

	public String toString() {
		return "Name: " + name.get() + 
				" Team: " + team.get() + 
				" Points: " + points.get() +
				" Rank: " + rank.get() +
				" User ID: " + userID.get() +
				" Clan ID: " + clanID.get() +
				" Inactive: " + inactive.get();
	}
}
