package application.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import application.Main;

public class RootLayoutController {

	private Main main;

	public void setMainApp(Main main) {
		this.main = main;
	}

	@FXML
	private void handleAbout() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("AddressApp");
		alert.setHeaderText("About");
		alert.setContentText("Author: Craig Lovell\nEmail: cxgslegend@gmail.com");

		alert.showAndWait();
	}

	@FXML
	private void handleExit() {
		System.exit(0);
	}
}