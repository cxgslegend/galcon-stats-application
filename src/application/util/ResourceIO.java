package application.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ResourceIO {
	private final String BASE_URL = "http://www.galcon.com/g2/logs/";
	private final String BASE_DATE = "2014-10-27";
	private final String LOGS_SAVE_LOCATION;

	private List<String> datesList;

	public ResourceIO(String logsSaveLocation) {
		LOGS_SAVE_LOCATION = logsSaveLocation;

		try {
			datesList = getDatesList();
		} 
		catch (ClassNotFoundException | IOException e1) {e1.printStackTrace();}
	}

	public void updateLogs() {
		for(int i = 0; i < datesList.size(); i++) {
			try {
				downloadResource(BASE_URL + datesList.get(i) + ".txt", 
						LOGS_SAVE_LOCATION + datesList.get(i) + ".txt");
			} 
			catch (IOException e) {e.printStackTrace();}			
		}
	}

	public List<String> getDatesList() throws ClassNotFoundException, IOException {
		List<String> datesList = new ArrayList<>();

		String baseStringDate = BASE_DATE;

		int year = Integer.parseInt(baseStringDate.replaceAll("-\\d{2}-\\d{2}", ""));
		int month = Integer.parseInt(baseStringDate.replaceAll("\\d{4}-", "").replaceAll("-\\d{2}", ""));
		int day = Integer.parseInt(baseStringDate.replaceAll("\\d{4}-\\d{2}-", ""));

		LocalDate baseDate = LocalDate.of(year, month, day);
		LocalDate rightNow = LocalDate.now();

		long daysBetween = ChronoUnit.DAYS.between(baseDate, rightNow) + 1;

		for(long i = 0; i < daysBetween; i++) {
			datesList.add(baseDate.plusDays(i).toString());
		}

		return datesList;
	}

	private void downloadResource(String downloadLocation, String saveLocation) throws IOException {
		if(checkIfResourceExists(saveLocation)) {return;}

		URL copyFromURL = new URL(downloadLocation);
		Scanner scan = new Scanner(copyFromURL.openStream());
		ReadableByteChannel readableByteChannel = Channels.newChannel(copyFromURL.openStream());
		FileOutputStream fileOutputStream = new FileOutputStream(saveLocation);
		fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);

		readableByteChannel.close();
		fileOutputStream.close();
	}

	private boolean checkIfResourceExists(String location) {
		Path path = Paths.get(location);

		return (Files.exists(path) ? true : false);
	}



}
