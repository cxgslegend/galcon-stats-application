package application.controller;

import java.io.File;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import application.Main;

public class SplashScreenDialogController {

	@FXML
	private TextField directoryLocationField;

	private Main main;
	private Stage dialogStage;
	private File file;

	public void setMainApp(Main main) {
		this.main = main;
	}

	@FXML
	private void initialize() {}

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;

		//this.dialogStage.getIcons().add(new Image("Need icon"));
	}

	@FXML
	private void handleChoose() {
		DirectoryChooser directoryChooser = new DirectoryChooser();
        file = directoryChooser.showDialog(main.getPrimaryStage());
        directoryLocationField.setText(file.getAbsolutePath());
	}

	@FXML
	private void handleOk() {
		if (isInputValid()) {
			main.setPreferenceKey("filePath", file.getPath());
			dialogStage.close();
		}
	}

	@FXML
	private void handleCancel() {
		System.exit(0);
	}

	private boolean isInputValid() {
		String errorMessage = "";

		if (directoryLocationField.getText() == null || directoryLocationField.getText().length() == 0 || !file.isDirectory()) {
			errorMessage += "No valid directory Location\n"; 
		}

		if (errorMessage.length() == 0) {
			return true;
		} 
		else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(dialogStage);
			alert.setTitle("Invalid Field");
			alert.setHeaderText("Please correct invalid field");
			alert.setContentText(errorMessage);

			alert.showAndWait();

			return false;
		}
	}
}